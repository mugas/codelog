<h1>Code Log</h1>

<p>I should have done this for long, but it's my <em>code long</em> that makes me help remember my progress and where I am in each topic day by day and how much time I dedicated to what.</p>

<h4>31/01//2019</h4>

<ol>
    <li>Learn about `buble sort` in #CS50 Harvard course.</li>
In bubble sort:
        <ul>
            <li>We move higher value elements towards the right and lower to the left</li>
            <li>Set the swaper to a non zero value</li>
            <li>Repeat until the count is 0</li>
            <li>Look at each adjacent pair that are not in order and swap them
        </ul>
    <li>Continue coding my `nuxtapp`</li>
        <ul>
            <li>Debug the app to render the posts. Was a problem with the folder name</li>
            <li>The posts now render, had some style but have to re style it to my way.
        </ul>
    <li>Create the project for my    Skill share class</li>
    <li>Work on more functions exercises</li>
</<ol>

<h4>1/02//2019</h4>

<ol>
    <li>Learn about `insertion sort` in #CS50 Harvard course.</li>
In insertion sort:
        <ul>
             <li>The idea of the algorithm is to build your sorted array in place, shifting elements out of the way if necessary to make room as you go</li>
        </ul>
    <li>Continue coding my `nuxtapp`.</li>
        <ul>
            <li>Start adding the code in storyblok for the posts to be editable there</li>
            <li>Have some error with `$storyblok.init(). Says it's undefined, the storyblok.
        </ul>
    <li>Searching for ideas for my css animations class. Need something fun to do but easy to create</li>
    <li>Learning more about async/await, promises and callback hell. Get ready to start my tutorial that will focus on API and promises</li>
    <li>Finish my focus about functions.Next step read and pratice with bookmarks I have been saving</li>
</ol>

<h4>3/02//2019</h4>

<ol>
    <li>On Sundays I work in my blog. I have been writing a post about control flow.Today I wrote about the break and continue.</li>
    <li>Still fixing my problem with nuxt.js and storyblok. All seems working till I create my hook with mounted...Still figuring out why. Was able to isolate the problem </li>
    <li>Start coding my class about CSS Animations and Transitions.See the page for grid and divide it by 4 elements </li>
</ol>

<h4>04/02/2019</h4>
<ol>
    <li>Learn about `linear search` in #CS50 Harvard course.</li>
In linear search:
        <ul>
            <li>The idea of the algorithm is to iterate across the aray from left to right, searching for a specified element</li>
        </ul>
    <li>Continue my nuxt app. It works, just can't put it editable on the server.</li>
    <li>Continue coding the project for my class</li>
</ol>

<h4>05/02/2019</h4>
<ol>
    <li>Learn about `binary search` in #CS50 Harvard course.</li>
    <li>Continue coding my nuxt app. Adding code and API for productions.</li>
    <li>A 1/4 of my CSS course is almost done..</li>
    <li>Work on API on js course</li>
    <li>Read more about curring and promises</li>
</ol>

<h4>06/02/2019</h4>

<ol>
    <li>Continue learning  about `binary search` in #CS50 Harvard course.</li>
    <li>Finnish my nuxt app/tutorial.Need now to code it my way</li>
    <li>Continue planning my class for SkillShare about CSS Animations</li>
    <li>More learning about promises and asyn/await</li>
</ol>

<h4>07/02/2019</h4>
<ol>
    <li>End my "binary search" lesson.In binary search:</li>
        <ul>
            <li>The idea is reduce the search area by half each time</li>
            <li>In order to do this, the array must be sorted before</li>
            <li>We calculate the middle point.If the larget is less than what's at the middle, repeat changing the end point to be just to the left of the middle.</li>
            <li>If the target is greater, repeat changing the start point to be just at the right of the middle</li>
        </ul>
    <li>My nuxt app is up and running but I am still fixing the live editing bug</li>
    <li>Start planning the music website</li>
    <li>Add gradients and text animations to my css class</li>
    <li>Work with async/await and promise</li>
</ol>

<h4>08/02/2019</h4>
<ol>
    <li>Summary of the algorithms in CS50git </li>
    <li>Start editing the  nuxt app.Today the about page adding the picture </li>
    <li>Coding one more part of the css class</li>
    <li>Starting my forkify project in js course</li>
    <li>More learning about async/await</li>
</ol>

<h4>10/02/2019</h4>
<ol>
    <li>Continue my post about control flow in JS </li>
    <li>Coding the favicon to my nuxt app </li>
    <li>Continue my class in css </li>
</ol>

<h4>11/02/2019</h4>
<ol>
    <li>Start learning about recursion in CS50</li>
    <li>Add my logo into the header</li>
    <li>Add transitions to images in css class</li>
    <li>Setting webpack for my forkify app</li>
    <li>More learning about async/await</li>
</ol>

<h4>12/02/2019</h4>
<ol>
    <li>Continue learning about recursion in CS50:</li>
        <ul>
            <li>Every recursive function has a base case,which when triggered will terminate the recursive process</li>
            <li>The recursive case which is where the recursion will actually occur</li>
        </ul>    
    <li>Plan the webpage for Ruckwater</li>
    <li>Add more animations to the page</li>
    <li>Continue setting webpack for my forkify app</li>
    <li>Read about questions for job interviews</li>
</ol>

<h4>13/02/2019</h4>
<ol>
    <li>Continue learning about recursion in CS50:</li>  
    <li>Start coding the post previews changes</li>
    <li>Add more animations to the page</li>
    <li>Continue setting webpack and also babel for my forkify app</li>
    <li>Read about questions for job interviews</li>
</ol>
